from django.apps import AppConfig


class YtServicesConfig(AppConfig):
    name = 'yt_services'
