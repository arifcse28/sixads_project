from django.contrib import admin
from .models import YTVideoModel, YTVideoDataModel

class YTVideoModelAdmin(admin.ModelAdmin):
    pass

class YTVideoDataModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(YTVideoModel, YTVideoModelAdmin)
admin.site.register(YTVideoDataModel, YTVideoDataModelAdmin)


