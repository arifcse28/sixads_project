from django.db import models


class YTTagModel(models.Model):
    name = models.CharField(verbose_name='Tag Name', max_length=255, default='')