from django.db import models


class YTChannelModel(models.Model):
    title = models.CharField(verbose_name='Channel Name', max_length=255, default='')
    description = models.TextField(verbose_name='Description', default='')
    published_at = models.DateTimeField(verbose_name='Published At')
    channel_id = models.CharField(verbose_name='Channel ID', max_length=255, default='')
    uploads_id = models.CharField(verbose_name='Uploads ID', max_length=255, default='')
