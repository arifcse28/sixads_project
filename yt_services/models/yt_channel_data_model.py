from django.db import models


class YTChannelDataModel(models.Model):
    channel = models.ForeignKey('YTChannelModel', verbose_name='Channel', on_delete=models.CASCADE, related_name='channel_data')
    created_at = models.DateTimeField(verbose_name='Created', auto_now_add=True)
    total_views = models.IntegerField(verbose_name='Total Views', default=0)
    total_comments = models.IntegerField(verbose_name='Total Comments', default=0)
    total_subscribers = models.IntegerField(verbose_name='Total Subscribers', default=0)
    total_videos = models.IntegerField(verbose_name='Total Videos', default=0)