from django.db import models


class YTVideoDataModel(models.Model):
    video = models.ForeignKey('YTVideoModel', verbose_name='Video', on_delete=models.CASCADE, related_name='video_data')
    created_at = models.DateTimeField(verbose_name='Created', auto_now_add=True)
    total_views = models.IntegerField(verbose_name='Total Views', default=0)
    total_comments = models.IntegerField(verbose_name='Total Comments', default=0)
    total_likes = models.IntegerField(verbose_name='Total Likes', default=0)
    total_dislikes = models.IntegerField(verbose_name='Total Dislikes', default=0)