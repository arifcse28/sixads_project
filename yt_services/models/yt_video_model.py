from django.db import models


class YTVideoModel(models.Model):
    channel = models.ForeignKey('YTChannelModel', verbose_name='Channel', on_delete=models.CASCADE,
                                related_name='channel_videos')
    title = models.CharField(verbose_name='Video Title', max_length=255, default='')
    description = models.TextField(verbose_name='Description', default='')
    video_id = models.CharField(verbose_name='Video ID', max_length=255, default='')
    published_at = models.DateTimeField(verbose_name='Published At')
    tags = models.ManyToManyField('YTTagModel', verbose_name='Tags',
                                  related_name='tags_videos')
