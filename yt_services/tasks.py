import time, logging
from __future__ import absolute_import, unicode_literals
from celery import task
from .utils import DBUtils

@task()
def task_save_channel_data():
    DBUtils().save_channel_data()
    return True

@task()
def task_save_channel_videos_data():
    DBUtils().save_channel_videos_data()
    return True