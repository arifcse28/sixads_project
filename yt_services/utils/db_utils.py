import logging
from ..models import YTTagModel, YTChannelModel, YTVideoModel, \
    YTChannelDataModel, YTVideoDataModel
from ..utils import YoutubeUtils


class DBUtils:


    def save_channel_data(self):
        youtube_util_obj = YoutubeUtils()
        channel_qs = YTChannelModel.objecsts.all()
        for channel in channel_qs:
            channel_data = youtube_util_obj.get_channel_data(channel_id=channel.channel_id)
            channel_data_qs = YTChannelDataModel.objects.create(
                channel = channel,
                total_views = channel_data['statistics']['viewCount'],
                total_comments = channel_data['statistics']['commentCount'],
                total_subscribers = channel_data['statistics']['subscriberCount'],
                total_videos = channel_data['statistics']['videoCount'],
            )
            logging.info(
                    '{channel_name} Data Saved'.format(channel_name=channel.title)
            )

    def save_channel_videos_data(self):
        youtube_util_obj = YoutubeUtils()
        channel_qs = YTChannelModel.objecsts.all()

        for channel in channel_qs:
            channel_videos = youtube_util_obj.get_channel_videos(channel_id=channel.channel_id)
            video_ids_list = list(map(lambda x:x['snippet']['resourceId']['videoId'], channel_videos))
            videos_data = youtube_util_obj.get_video_details(video_ids=video_ids_list)

            for video in videos_data:
                video_qs = YTVideoModel.objects.filter(channel__id=channel.id, video_id=video['id'])
                if video_qs.exists():
                    video_obj = video_qs.first()
                else:
                    tags_qs = []
                    for tag in video['snippet']['tags']:
                        obj, created = YTTagModel.objects.get_or_create(
                                name=tag
                        )
                        tags_qs += [obj]
                    video_obj = YTVideoModel.objects.create(
                            channel = channel,
                            title = video['snippet']['title'],
                            description = video['snippet']['description'],
                            video_id = video['id'],
                            published_at = video['snippet']['publishedAt'],
                    )
                    video_obj.tags.add(tags_qs)
                    video_obj.save()
                YTVideoDataModel.objects.create(
                    video=video_obj,
                    total_views=video['statistics']['viewCount'],
                    total_comments=video['statistics']['commentCount'],
                    total_likes=video['statistics']['likeCount'],
                    total_dislikes=video['statistics']['dislikeCount'],
                )
                logging.info('Video Data Saved for channel: {channel}'.format(channel=channel.title))