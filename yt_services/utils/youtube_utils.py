from django.conf import settings
from googleapiclient.discovery import build

class YoutubeUtils:
    YOUTUBE_API_CLIENT = 'youtube'
    YOUTUBE_API_VERSION = 'v3'


    def __init__(self):
        self.yt_build = build(self.YOUTUBE_API_CLIENT,
                   self.YOUTUBE_API_VERSION,
                   developerKey=settings.YOUTUBE_API_KEY)

    def get_channel_data(self, channel_id):
        channel_details = self.yt_build.channels().list(
                id=channel_id,
                part='snippet,contentDetails,statistics'
        ).execute()
        return channel_details['items'][0]

    def get_channel_videos(self, channel_id):
        channel_details = self.get_channel_data(channel_id=channel_id)
        playlist_id = channel_details['contentDetails']['relatedPlaylists']['uploads']
        videos = []
        next_page_token = None
        while True:
            playlist_videos = self.yt_build.playlistItems().list(
                    playlistId=playlist_id,
                    part='snippet',
                    maxResults=50,
                    pageToken=next_page_token
            ).execute()
            videos += playlist_videos['items']
            next_page_token = playlist_videos.get('nextPageToken', None)

            if next_page_token is None:
                break
        return videos

    def get_video_details(self, video_ids):
        video_details = []
        for i in range(0, len(video_ids), 50):
            videos = self.yt_build.videos().list(
                    id=','.join(video_ids[i:i+50]),
                    part='snippet,contentDetails,statistics'
            ).execute()
            video_details += videos['items']
        return video_details
